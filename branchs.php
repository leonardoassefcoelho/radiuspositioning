 <?php

    $branches = file_get_contents("branches.json");
    $branches = json_decode($branches);

    $new_branches = array();

    foreach ($branches as $branch) {
        $branch_name = $branch->branchnick;
        unset($branch->branchnick);
        unset($branch->channel);
        unset($branch->commission);
        unset($branch->agentname);
        unset($branch->managername);
        unset($branch->branchapicode);
        unset($branch->branchcategory);
        unset($branch->acceptedproducts);
        unset($branch->allowedproducts);
        unset($branch->validareacodes);
        unset($branch->validzipcodes);
        unset($branch->emails);
        unset($branch->telephone);
        unset($branch->mobilephone);
        unset($branch->priority);
        unset($branch->alwayssendemail);
        unset($branch->filters);
        unset($branch->branchdefault);
        unset($branch->validationsteps);
        unset($branch->fakebid);
        unset($branch->bureaudocuments);
        unset($branch->showdraft);
        unset($branch->layoutprops);

        $curl = curl_init();

        $userAgent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2';

        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);

        $url = sprintf("https://nominatim.openstreetmap.org/?format=json&addressdetails=1&limit=1&q=%s&city=%s&country=brazil&state=%s", urlencode($branch->address), urlencode($branch->city), urlencode($branch->state));

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response);

        if ($response && $response[0]->lat && $response[0]->lon) {
            $branch->latitude = $response[0]->lat;
            $branch->longitude = $response[0]->lon;

            $new_branches[$branch_name] = $branch;
        }
    }

    print_r($new_branches);

    $branches_json = json_encode($new_branches);

    file_put_contents("branches_new.json", utf8_encode($branches_json));
