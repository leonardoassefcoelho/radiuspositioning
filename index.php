<?php
$branches = json_decode(file_get_contents("branches.json"));

$userLocation = array(
    "lat" => deg2rad(-22.0184566),
    "long" => deg2rad(-47.9310767),
);
$distance = 300;

//Constantes
$pi = deg2rad(180);
$earthRadius = 6371.01;

//Raio de busca em KM
$angularRadius = $distance / $earthRadius;

$minLat = $userLocation["lat"] - $angularRadius;
$maxLat = $userLocation["lat"] + $angularRadius;

$latT = asin(sin($userLocation["lat"]) / cos($angularRadius));

$deltaLong = asin(sin($angularRadius) / cos($userLocation["lat"]));

$minLong = $userLocation["long"] - $deltaLong;
$maxLong = $userLocation["long"] + $deltaLong;

//Busca em cada item da array se está dentro do raio e imprime o nome do item.
foreach ($branches as $branch => $coordinates) {
    $lat = deg2rad($coordinates->latitude);
    $long = deg2rad($coordinates->longitude);

    if (($lat >= $minLat && $lat <= $maxLat) && ($long >= $minLong && $long <= $maxLong)) {
        if (acos(sin($userLocation["lat"]) * sin($lat) + cos($userLocation["lat"]) * cos($lat) * cos($long - ($userLocation["long"])))) {
            echo $branch . "\n";
        }
    } else {
        $hasBranchs = true;
    }
}
